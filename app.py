# general libraries
import numpy as np
import logging

# deep learning libraries
from keras.models import load_model
from keras import backend as K

# ticket class
import Ticket
# import User

from DBHelper import DBHelper as dbh

# natural language processing libraries
import nltk
nltk.download('punkt')
from nltk.stem.snowball import ItalianStemmer

# telegram libraries
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, ConversationHandler)
from telegram import ReplyKeyboardMarkup

# database libraries
import sqlite3

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# global variable to store one temporary ticket for each different user
tickets = []
             
##################################################
################ DEEP LEARNING ###################
##################################################

# memory clean
K.clear_session()

# create an Italian language stemmer
stemmer = ItalianStemmer()
    
######## PROBLEM SECTION #########################

import csv

with open('problem_words.csv', 'r') as csvfile:
    problem_words = [row[0] for row in csv.reader(csvfile, delimiter=',')]

with open('problem_classes.csv', 'r') as csvfile:
    problem_classes = [row[0] for row in csv.reader(csvfile, delimiter=',')]
    
with open('problem_documents.csv', 'r') as csvfile:
    problem_documents = [row[0] for row in csv.reader(csvfile, delimiter=',')]

# no answer class index
problem_no_answer_index = [i for i,x in enumerate(problem_classes) if x == "noanswer"]

# load a deep learn model
problem_model = load_model('problem_model.h5')
problem_model._make_predict_function()

def stem_and_tokenize_sentence(sentence):
    # tokenize the pattern
    sentence_words = nltk.word_tokenize(sentence)
    # stem each word
    sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
    return sentence_words

# return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
def problem_bow(sentence, problem_words, show_details=False):
    # tokenize the pattern
    sentence_words = stem_and_tokenize_sentence(sentence)
    # bag of words
    bag = [0]*len(problem_words)  
    for s in sentence_words:
        for i,w in enumerate(problem_words):
            if w == s: 
                bag[i] = 1
    return(np.array(bag))

# set the error threshold to understand 
PROBLEM_ERROR_THRESHOLD = 0.5

# classify a new sentence in a category
def problem_classify(sentence):
    # generate probabilities from the model
    
    # vector with 0s for each word in the training set and 
    # 1s for each word in the sentence AND in the training set 
    bag_of_words = problem_bow(sentence, problem_words)

    # set the input of the deep learning model 
    bow_capacity = len(bag_of_words)
    doc_capacity = len(problem_documents)-2
    zero_matrix = np.zeros([doc_capacity, bow_capacity])
    
    dl_input = np.vstack((bag_of_words,zero_matrix))
    
    # categorization probabilities
    results = problem_model.predict(dl_input)[0]

    # filter out predictions below a threshold
    results = [[i,r] for i,r in enumerate(results) if ((r > PROBLEM_ERROR_THRESHOLD)  or (i == problem_no_answer_index)) ]
    
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append((problem_classes[r[0]], r[1]))
        
    if len(return_list) == 0:
        return_list.append(('noanswer',0))
           
    # return tuple of intent and probability
    return return_list

############### FAQ SECTION ######################

with open('faq_words.csv', 'r') as csvfile:
    faq_words = [row[0] for row in csv.reader(csvfile, delimiter=',')]

with open('faq_classes.csv', 'r') as csvfile:
    faq_classes = [row[0] for row in csv.reader(csvfile, delimiter=',')]
    
with open('faq_documents.csv', 'r') as csvfile:
    faq_documents = [row[0] for row in csv.reader(csvfile, delimiter=',')]

# no answer class index
faq_no_answer_index = [i for i,x in enumerate(faq_classes) if x == "noanswer"]

# load a deep learn model
faq_model = load_model('faq_model.h5')
faq_model._make_predict_function()

# return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
def faq_bow(sentence, faq_words, show_details=False):
    # tokenize the pattern
    sentence_words = stem_and_tokenize_sentence(sentence)
    # bag of words
    bag = [0]*len(faq_words)  
    for s in sentence_words:
        for i,w in enumerate(faq_words):
            if w == s: 
                bag[i] = 1
    return(np.array(bag))

# set the error threshold to understand 
FAQ_ERROR_THRESHOLD = 0.6

# classify a new sentence in a category
def faq_classify(sentence):
    # generate probabilities from the model
    
    # vector with 0s for each word in the training set and 
    # 1s for each word in the sentence AND in the training set 
    bag_of_words = faq_bow(sentence, faq_words)

    # set the input of the deep learning model 
    bow_capacity = len(bag_of_words)
    doc_capacity = len(faq_documents)-2
    zero_matrix = np.zeros([doc_capacity, bow_capacity])
    
    dl_input = np.vstack((bag_of_words,zero_matrix))
    
    # categorization probabilities
    results = faq_model.predict(dl_input)[0]

    # filter out predictions below a threshold
    results = [[i,r] for i,r in enumerate(results) if ((r > FAQ_ERROR_THRESHOLD)  or (i == faq_no_answer_index)) ]
    
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append((faq_classes[r[0]], r[1]))
        
    if len(return_list) == 0:
        return_list.append(('noanswer',0))
           
    # return tuple of intent and probability
    return return_list

##################################################
########### FINITE STATE MACHINE #################
##################################################

# states list
START, INSERT_SURNAME, REPEAT_SURNAME, PROBLEM_INFO, PROBLEM, INFO, DONE, \
FEEDBACK, ADD_FEEDBACK, BLOCKING_PROBLEM, PERVASIVE_PROBLEM, \
SIST_OP, SCREENSHOT, FINAL_FEEDBACK, FAQ, INFO_DONE = range(16)

# predefined keyboards
reply_keyboard_1 = [['Voglio segnalare un problema'], 
                     ['Ho bisogno di alcune informazioni']]

reply_keyboard_2 = [['Lascia un Feedback'],
                    ['Fine']]

reply_keyboard_3 = [['Si', 'No']]

reply_keyboard_4 = [['Si', 'No'],
                    ['Non saprei dirlo']]

reply_keyboard_5 = [['No']]
                
markup_1 = ReplyKeyboardMarkup(reply_keyboard_1, resize_keyboard=True, one_time_keyboard=True)
markup_2 = ReplyKeyboardMarkup(reply_keyboard_2, resize_keyboard=True, one_time_keyboard=True)
markup_3 = ReplyKeyboardMarkup(reply_keyboard_3, resize_keyboard=True, one_time_keyboard=True)
markup_4 = ReplyKeyboardMarkup(reply_keyboard_4, resize_keyboard=True, one_time_keyboard=True)
markup_5 = ReplyKeyboardMarkup(reply_keyboard_5, resize_keyboard=True, one_time_keyboard=True)

##################################################
def start(bot, update):
    
    logging.info('start state')
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    # chat identifier
    chat_id = update.message.chat.id  
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)

    # list of all temporary tickets
    global tickets
    
    # looking for an already existing temporary ticket of this user
    myTicket = [x for x in tickets if x.chat_id == chat_id]
    
    # if does not exist, create a new one
    if len(myTicket) == 0:
        myTicket = Ticket.Ticket(user_id)
        myTicket.chat_id = chat_id
        tickets.append(myTicket)
        
    # else reset it
    else:
        myTicket = myTicket[0]
        myTicket.reset()
         
    # search a user name by his chat identifier          
    user_name = dbh.select_user_name_by_id(conn,chat_id).capitalize()
     
    logging.info(user_name)
   
    # if no user has been found, ask for his name and surname
    if user_name == "None":    
        # text that will be sent to the user
        text_reply = 'Ciao , sono il tuo assistente virtuale! Scrivi qui sotto il tuo Nome e Cognome'
        bot.sendMessage(chat_id, text_reply)
        
        return INSERT_SURNAME
    
    # else ask about his issue
    else:
        # text that will be sent to the user
        text_reply = "Ciao {0}! Come posso aiutarti?".format(user_name)
        # send message to the user
        update.message.reply_text(text_reply, reply_markup=markup_1)
        
        return PROBLEM_INFO

##################################################
def insert_surname(bot,update, user_data):
    
    logging.info('insert surname state')

    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    # received text    
    name_surname = update.message.text
    
    # get the user id through his name and surname        
    user_id = dbh.select_user_id_by_name_and_surname(conn,name_surname)
    
    # if cannot find the surname in the user table, ask it again
    if user_id == "None":
        # text that will be sent to the user
        text_reply = "Scusa, ma non ho trovato il tuo nome in archivio. Potresti riscrivere il tuo Nome e Cognome?"
        # send message to the user
        update.message.reply_text(text_reply)
        
        return REPEAT_SURNAME
    
    # else update the chat_id of the user    
    else:
        # received text 
        chat_id = update.message.chat.id

        # insert the chat_id in the users table
        user_update =  (chat_id,user_id)
        dbh.update_user_chat_id(conn,user_update)
        
        user_name = dbh.select_user_name_by_chat_id(conn,chat_id).capitalize()
        
        # text that will be sent to the user
        text_reply = 'Ciao {0}! Di cosa hai bisogno?'.format(user_name)
        # send message to the user
        update.message.reply_text(text_reply, reply_markup=markup_1)
        
        return PROBLEM_INFO
    
##################################################
def repeat_surname(bot,update, user_data):
    
    logging.info('repeat surname state')

    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    # received text    
    name_surname = update.message.text

    # get the user id through his name and surname        
    user_id = dbh.select_user_id_by_name_and_surname(conn,name_surname)
    
    # if cannot find the surname in the user table, ask it again
    if user_id == "None":
        # text that will be sent to the user
        text_reply = "Scusa, non ho capito. Potresti riscrivere il tuo Nome e Cognome?"
        # send message to the user
        update.message.reply_text(text_reply)
        
        return REPEAT_SURNAME
        
    else:
        # received text 
        chat_id = update.message.chat.id

        # insert the chat_id in the users table
        user_update =  (chat_id,user_id)
        dbh.update_user_chat_id(conn,user_update)
        
        user_name = dbh.select_user_name_by_chat_id(conn,chat_id).capitalize()
        
        # text that will be sent to the user
        text_reply = 'Ciao {0}! Di cosa hai bisogno?'.format(user_name)
        # send message to the user
        update.message.reply_text(text_reply, reply_markup=markup_1)
        
        return PROBLEM_INFO

##################################################
def problem_info(bot,update, user_data):
    
    logging.info('problem info state')
    
    # received text
    text = update.message.text
    
    # list of all temporary tickets
    global tickets
    
    if text == 'Voglio segnalare un problema':
        # text that will be sent to the user
        text_reply = "Ok, descrivi qui sotto il tuo problema"
        # send message to the user
        update.message.reply_text(text_reply)
        
        return PROBLEM
    
    elif text == "Ho bisogno di alcune informazioni":
        # text that will be sent to the user
        text_reply = "Di quali informazioni hai bisogno?"
        # send message to the user
        update.message.reply_text(text_reply)
        
        return INFO
    
    else:
        # text that will be sent to the user
        text_reply = "Scusa, non ho capito. Di cosa hai bisogno?"
        # send message to the user
        update.message.reply_text(text_reply,reply_markup=markup_1)
        
        return PROBLEM_INFO
       
##################################################
def problem(bot,update, user_data):
    
    logging.info('problem state')
    
    # chat identifier
    chat_id = update.message.chat.id
    # received text
    text = update.message.text
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # list of all temporary tickets
    global tickets
    
    # looking for an already existing temporary ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id]
    
    # if does not exist, create a new one
    if len(myTicket) == 0:
        myTicket = Ticket.Ticket(user_id)
        myTicket.chat_id = chat_id
        tickets.append(myTicket)
        
    # else reset it
    else:
        myTicket = myTicket[0]
        myTicket.reset()
    
    # classify the issue
    results = problem_classify(text)
    
    logging.info(results)
    
    # add corpus to temporary ticket
    myTicket.corpus = text    
    # add category to temporary ticket
    myTicket.category = results[0][0]
    
    # message not classified
    if myTicket.category == 'noanswer':
        
        # if message not classified less than 3 times
        if myTicket.count < 2:
            # increment misunderstanding counter
            myTicket.count += 1
            
            # text that will be sent to the user
            text_reply = "Mi dispiace ma non ho capito. Potresti essere più preciso nella descrizione del problema?"
            # send message to the user
            update.message.reply_text(text_reply)
            
            return PROBLEM
        
        # else open a ticket with unclassified category
        else:
            # add category to temporary ticket
            myTicket.category = "unclassified"
            
            # text that will be sent to the user
            text_reply = "Mi dispiace, ma non riesco proprio a capire. Ho comunque aperto un ticket che verrà gestito il prima possibile"
            # send message to the user
            update.message.reply_text(text_reply, reply_markup=markup_2)
            
            return FEEDBACK
    
    # ticket correctly classified
    else:
        # HW or SW problem
        
        # text that will be sent to the user
        text_reply = "Il problema è bloccante? E' necessario un intervento in giornata?"
        # send message to the user
        update.message.reply_text(text_reply, reply_markup=markup_3)
        
        return BLOCKING_PROBLEM
        
##################################################
def blocking_problem(bot,update,user_data):
    
    logging.info('blocking problem state')
    
    # chat identifier
    chat_id = update.message.chat.id
    # received text
    text = update.message.text
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # looking for an already existing temporary ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
     
    #add additional corpus to temporary ticket
    myTicket.corpus = myTicket.corpus + "\n\nProblema bloccante: " + text
    
    # text that will be sent to the user
    text_reply = "Stai riscontrando solo tu questo problema?"
    # send message to the user
    update.message.reply_text(text_reply, reply_markup=markup_4)
    
    return PERVASIVE_PROBLEM

##################################################
def pervasive_problem(bot,update,user_data):
    
    logging.info('pervasive problem state')
    
    # chat identifier
    chat_id = update.message.chat.id
    # received text
    text = update.message.text
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # looking for an already existing temporary ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
     
    #add additional corpus to temporary ticket
    myTicket.corpus = myTicket.corpus + "\n\nProblema pervasivo: " + text
    
    # HW problem
    if myTicket.category == "HW - Richiesta Nuovo HW" or myTicket.category == "HW - Accessori"\
        or myTicket.category == "HW - PC":
            
        # text that will be sent to the user
        text_reply = "Grazie per la collaborazione, è stato aperto un ticket che verrà gestito il prima possibile"
        # send message to the user
        update.message.reply_text(text_reply, reply_markup=markup_2)
        
        return FEEDBACK 
    
    # SW problem + HW - rete
    else:
        
        # text that will be sent to the user
        text_reply = "Quale sistema operativo stai utilizzando? Possibilmente indica anche la versione"
        # send message to the user
        update.message.reply_text(text_reply)
        
        return SIST_OP 
    
##################################################        
def sist_op(bot,update,user_data):
    
    logging.info('sist_op state')
        
    # chat identifier
    chat_id = update.message.chat.id
    # received text
    text = update.message.text
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # looking for an already existing temporary ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
     
    #add additional corpus to temporary ticket
    myTicket.corpus = myTicket.corpus + "\n\nSistema operativo: " + text
    
    # text that will be sent to the user
    text_reply = "Se ritieni possa essere utile, allega uno screenshot. Altrimenti seleziona 'No'"
    # send message to the user
    update.message.reply_text(text_reply,reply_markup=markup_5)
    
    return SCREENSHOT

##################################################       
def screenshot(bot,update,user_data):
    
    logging.info('screenshot state')
        
    # chat identifier
    chat_id = update.message.chat.id
    
    # received text
    text = update.message.text
    
    screenshot_file = None
    
    if text == None:
        screenshot_file = bot.getFile(update.message.photo[-1].file_id)
        screenshot_url = screenshot_file['file_path']
    
    print (screenshot_file)
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # looking for an already existing temporary ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
    
    if screenshot_file != None:
        #add additional corpus to temporary ticket
        myTicket.corpus = myTicket.corpus + "\n\nScreenshot: " + screenshot_url
    
    # text that will be sent to the user
    text_reply = "Grazie per la collaborazione, è stato aperto un ticket che verrà gestito il prima possibile"
    # send message to the user
    update.message.reply_text(text_reply,reply_markup=markup_2)
    
    return FEEDBACK

##################################################
def feedback(bot,update,user_data):
    
    logging.info('feedback state')

    # chat identifier    
    chat_id = update.message.chat.id
    # received text
    text = update.message.text
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # list of all temporary tickets
    global tickets
    
    # looking for an already existing temporary ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)  
    
    logging.info(myTicket.category)
    
    if text == "Lascia un Feedback":
        # text that will be sent to the user    
        text_reply = "Il tuo ticket è stato classificato nella categoria " + myTicket.category + ". E' esatta?"
        
        # send message to the user
        update.message.reply_text(text_reply, reply_markup=markup_3)
        
        return ADD_FEEDBACK
    
    else:
        # add ticket to the tickets table
        ticket = myTicket.asArray()
        dbh.insert_ticket(conn, ticket)
        
        # close chatbot
        logging.info('Chatbot closed')
        return ConversationHandler.END

##################################################
def add_feedback(bot,update,user_data):
    
    logging.info('add_feedback state')
    
    # chat identifier
    chat_id = update.message.chat.id  
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)

    # list of all temporary tickets
    global tickets
    
    # looking for an already temporary existing ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
    
    # add feedback to temporary ticket
    myTicket.feedback = update.message.text
    
    # text that will be sent to the user   
    text_reply = "Grazie per la collaborazione. Desideri lasciare un feedback aggiuntivo?"
    # send message to the user
    update.message.reply_text(text_reply, reply_markup=markup_3)
    
    return FINAL_FEEDBACK

##################################################
def final_feedback(bot,update,user_data):
    
    logging.info('final_feedback state')
        
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    # chat identifier
    chat_id = update.message.chat.id
    # received text
    received_text = update.message.text
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)

    # list of all temporary tickets
    global tickets
    
    # looking for an already temporary existing ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]

    # if the user says "No", then open the ticket without an additional feedback
    if received_text == "No":
        # add ticket to tickets table
        ticket = myTicket.asArray()
        dbh.insert_ticket(conn, ticket)
        
        # text that will be sent to the user  
        text_reply = "Grazie, buona giornata!"
        # send message to the user
        update.message.reply_text(text_reply)
        
        # close chatbot
        logging.info('Chatbot closed')
        return ConversationHandler.END
    
    # else let the user leave an additional feedback
    else:
        # text that will be sent to the user  
        text_reply = "Inserisci qui sotto il tuo commento"
        # send message to the user
        update.message.reply_text(text_reply)
        
        return DONE    
    
##################################################
def info(bot,update,user_data):
    
    logging.info('info state')
    
    # chat identifier      
    chat_id = update.message.chat.id
    # received text
    text = update.message.text
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # list of all temporary tickets
    global tickets
    
    # classify the issue
    results = faq_classify(text)
    
    logging.info(results)

    # looking for an already temporary existing ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
                
    # add corpus to temporary ticket
    myTicket.corpus = text
    # add category to temporary ticket
    myTicket.category = results[0][0]
    
    # message not classified
    if myTicket.category == 'noanswer':
        
        # if message not classified less than 3 times
        if myTicket.count < 2:
            # increment misunderstanding counter
            myTicket.count += 1
            
            # text that will be sent to the user
            text_reply = "Mi dispiace ma non ho capito. Potresti essere più preciso nella descrizione del problema?"
            # send message to the user
            update.message.reply_text(text_reply)
            
            return INFO
        
        # else open a ticket with unclassified category
        else:
            # add category to temporary ticket
            myTicket.category = "unclassified"
            
            # text that will be sent to the user
            text_reply = "Mi dispiace, ma non ho una risposta alla tua domanda. Desideri aprire un ticket?"
            # send message to the user
            update.message.reply_text(text_reply, reply_markup=markup_3)
            
            return INFO_DONE
    
    # office1 category
    elif myTicket.category == "Office1":
        
        # text that will be sent to the user
        text_reply_1 = "Da outlook: File -> Informazioni -> Risposte automatiche (Fuori sede) -> impostare come da screenshot:"
        text_reply_2 = "Scegliere intervallo di tempo, scrivere un messaggio personalizzato, scriverne uno anche per gli utenti esterni all’organizzazione, dare ok."
        text_reply_3 = "Il tuo problema è stato risolto?"

        
        # send message to the user
                
        update.message.reply_text(text_reply_1)
        
        bot.send_photo(chat_id=chat_id, photo=open('pics/office1.jpg', 'rb'))
        
        update.message.reply_text(text_reply_2)
        
        update.message.reply_text(text_reply_3, reply_markup=markup_3)

        return FAQ
    
    # office2 category
    elif myTicket.category == "Office2":
                
        # text that will be sent to the user
        text_reply_1 = "Da Outlook: File -> Informazioni -> Impostazioni cassetta postale -> Pulisci vecchi elementi -> impostare come da screenshot:"
        text_reply_2 = "Scegliere la propria mail, scegliere una data (verranno archiviate le mail antecedenti), selezionare l’archivio dentro al quale si vogliono inserire le mail (se non esiste scegliere un nome e scriverlo come da screenshot e l’archivio viene creato), dare ok."
        text_reply_3 = "Il tuo problema è stato risolto?"

        pic = open('pics/office2.png', 'rb')
        
        # send message to the user
                
        update.message.reply_text(text_reply_1)
        
        bot.send_photo(chat_id=chat_id, photo=pic)
        
        update.message.reply_text(text_reply_2)
        
        update.message.reply_text(text_reply_3, reply_markup=markup_3)

        return FAQ
    
    # connessione category
    elif myTicket.category == "Rete":
                
        # text that will be sent to the user
        text_reply_1 = "Chiedere all’IT l’attivazione tramite una mail a servicerequest@aton.eu"
        text_reply_2 = "Aprire Forticlient e impostare la VPN con i seguenti paramentri da: remote access -> ingranaggio -> add new connection"
        text_reply_3 = "Inserire il proprio nome e cognome (con spazio tra uno e l’altro) e utilizzare la password del pc (di dominio) al momento della connessione"
        text_reply_4 = "Il tuo problema è stato risolto?"
        pic = open('pics/office2.png', 'rb')
        
        # send message to the user
                
        update.message.reply_text(text_reply_1)
        update.message.reply_text(text_reply_2)
        
        bot.send_photo(chat_id=chat_id, photo=pic)
        
        update.message.reply_text(text_reply_3)
        update.message.reply_text(text_reply_4, reply_markup=markup_3)
        
        return FAQ
    
    # connessione category
    elif myTicket.category == "Pc1":
                
        # text that will be sent to the user
        text_reply_1 = "Pannello di controllo -> opzioni risparmio energia -> Specifica impostazioni di disattivazione dello schermo (menu di sinistra) -> scegliere una combinazione."
        text_reply_2 = "Il tuo problema è stato risolto?"
        # send message to the user
        update.message.reply_text(text_reply_1)
        update.message.reply_text(text_reply_2, reply_markup=markup_3)
        
        return FAQ
    
    # connessione category
    elif myTicket.category == "Pc2":
                
        # text that will be sent to the user
        text_reply_1 = "Pannello di controllo -> Programmi e funzionalità -> selezionare il programma che si vuole disinstallare -> Disinstalla"
        text_reply_2 = "Il tuo problema è stato risolto?"
        # send message to the user
        update.message.reply_text(text_reply_1)
        update.message.reply_text(text_reply_2, reply_markup=markup_3)
        
        return FAQ

##################################################
def info_done(bot,update,user_data):
    
    logging.info('info_done state')
        
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    # add corpus to temporary ticket
    chat_id = update.message.chat.id
    # received text
    received_text = update.message.text
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)

    # list of all temporary tickets
    global tickets
    
    # looking for an already temporary existing ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]

    # do not open a ticket
    if received_text == "No":
        
        # text that will be sent to the user
        text_reply = "Ok, buona giornata!"
        # send message to the user
        update.message.reply_text(text_reply)
            
        # close chatbot
        logging.info('Chatbot closed')
        return ConversationHandler.END
    
    # open a ticket
    else:
        # add ticket to tickets table
        ticket = myTicket.asArray()
        dbh.insert_ticket(conn, ticket)
        
        # text that will be sent to the user
        text_reply = "Ok, ho aperto un ticket che verrà gestito il prima possibile. Buona giornata!"
        # send message to the user
        update.message.reply_text(text_reply)
        
        # close chatbot
        logging.info('Chatbot closed')
        return ConversationHandler.END     
     
##################################################
def faq(bot,update,user_data):
    
    logging.info('faq state')
    
     # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    # chat identifier
    chat_id = update.message.chat.id
    # received text
    received_text = update.message.text
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # list of all temporary tickets    
    global tickets
    
    # looking for an already temporary existing ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]
    
    # unsolved issue
    if received_text == "No":
        # text that will be sent to the user
        text_reply = "Mi dispiace ma non possiedo le informazioni da te richieste. Desideri aprire un ticket?"
        # send message to the user
        update.message.reply_text(text_reply, reply_markup=markup_3)
        
        return INFO_DONE
    
    # solved issue
    else:
        # add ticket to tickets table
        ticket = myTicket.asArray()
        dbh.insert_ticket(conn, ticket)
        
        # text that will be sent to the user
        text_reply = "Perfetto, buona giornata!"
        # send message to the user
        update.message.reply_text(text_reply)
        
        # close chatbot
        logging.info('Chatbot closed')
        return ConversationHandler.END      
        
##################################################
def done(bot, update, user_data):
    
    logging.info('done state')
    
    # chat identifier
    chat_id = update.message.chat.id
    
    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    user_id = dbh.select_user_id_by_chat_id(conn,chat_id)
    
    # list of all temporary tickets
    global tickets
    
    # looking for an already temporary existing ticket of this user
    myTicket = [x for x in tickets if x.user_id == user_id][0]

    # Connect to database
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
     
    # add additional feedback to temporary ticket
    myTicket.add_feedback = update.message.text
    
    # add ticket to tickets table
    ticket = myTicket.asArray()
    dbh.insert_ticket(conn, ticket)
    
    # text that will be sent to the user
    text_reply = "Grazie, buona giornata!"
    # send message to the user
    update.message.reply_text(text_reply)
        
    # close chatbot
    logging.info('Chatbot closed')
    return ConversationHandler.END

##################################################
#################### MAIN ########################
##################################################
def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater("623919577:AAEx8j-8n7dNZ0355Dv-oRbPsGWoxAtMzco", workers=32)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    
    # Database setup     
    database = "pythonsqlite.db" 
    conn = sqlite3.connect(database)
    
    sql_create_users_table = """ CREATE TABLE IF NOT EXISTS users (
                                    user_id integer PRIMARY KEY AUTOINCREMENT,
                                    chat_id text,
                                    name text NOT NULL,
                                    surname text NOT NULL,
                                    role text NOT NULL
                                ); """
 
    sql_create_tickets_table = """CREATE TABLE IF NOT EXISTS tickets (
                                    ticket_id integer PRIMARY KEY AUTOINCREMENT,
                                    user_id integer NOT NULL,
                                    chat_id integer NOT NULL,
                                    timestamp text NOT NULL,
                                    category text NOT NULL,
                                    corpus text NOT NULL,
                                    feedback text,
                                    add_feedback text,
                                    FOREIGN KEY (user_id) REFERENCES users (user_id)
                                );"""
    
    if conn is not None:
        # create projects table
        dbh.create_table(conn, sql_create_users_table)
        # create tasks table
        dbh.create_table(conn, sql_create_tickets_table)
    
    logging.info('Chatbot started')

    # Add conversation handler with the states
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            START: [MessageHandler(Filters.text,
                                    start,
                                    pass_user_data=True),
                    ],
            INSERT_SURNAME: [MessageHandler(Filters.text,
                                    insert_surname,
                                    pass_user_data=True),
                    ],
            REPEAT_SURNAME: [MessageHandler(Filters.text,
                                    repeat_surname,
                                    pass_user_data=True),
                    ],
            PROBLEM_INFO: [MessageHandler(Filters.text,
                                    problem_info,
                                    pass_user_data=True),
                    ],
            PROBLEM: [MessageHandler(Filters.text,
                                    problem,
                                    pass_user_data=True),
                    ],
            BLOCKING_PROBLEM: [MessageHandler(Filters.text,
                                    blocking_problem,
                                    pass_user_data=True),
                    ],
            PERVASIVE_PROBLEM: [MessageHandler(Filters.text,
                                    pervasive_problem,
                                    pass_user_data=True),
                    ],
            SIST_OP: [MessageHandler(Filters.text,
                                    sist_op,
                                    pass_user_data=True),
                    ],
            SCREENSHOT: [MessageHandler(Filters.text | Filters.photo,
                                    screenshot,
                                    pass_user_data=True),
                    ],                                     
            INFO: [MessageHandler(Filters.text,
                                    info,
                                    pass_user_data=True),
                    ],                                
            DONE: [MessageHandler(Filters.text,
                                    done,
                                    pass_user_data=True),
                    ],
            FEEDBACK: [MessageHandler(Filters.text,
                                    feedback,
                                    pass_user_data=True),
                    ],
            ADD_FEEDBACK: [MessageHandler(Filters.text,
                                    add_feedback,
                                    pass_user_data=True),
                    ],
            FINAL_FEEDBACK: [MessageHandler(Filters.text,
                                    final_feedback,
                                    pass_user_data=True),
                    ],
            FAQ: [MessageHandler(Filters.text,
                                    faq,
                                    pass_user_data=True),
                    ],
            INFO_DONE: [MessageHandler(Filters.text,
                                    info_done,
                                    pass_user_data=True),
                    ],
            },

        fallbacks=[]
    )

    dp.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()
    
##################################################
##################################################
##################################################

if __name__ == '__main__':
    main()