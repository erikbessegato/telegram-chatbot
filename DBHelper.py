import sqlite3
from sqlite3 import Error
import logging

class DBHelper:
        
    def create_table(conn, create_table_sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """
        try:
            c = conn.cursor()
            c.execute(create_table_sql)
        except Error as e:
            print(e)

    def insert_user(conn, user): 
        """
        Create a new project into the projects table
        :param conn:
        :param project:
        :return: project id
        """
        sql = ''' INSERT INTO users(user_id,chat_id,name,surname,role)
                  VALUES(?,?,?,?,?) '''
        cur = conn.cursor()
        with conn:
            cur.execute(sql, user)
        return cur.lastrowid
    
    
    def insert_ticket(conn, ticket):
        """
        Create a new ticket
        :param conn:
        :param ticket:
        :return:
        """
        sql = ''' INSERT INTO tickets(ticket_id,user_id,chat_id,timestamp,category,corpus,feedback,add_feedback)
                  VALUES(?,?,?,?,?,?,?,?) '''
        cur = conn.cursor()
        with conn:
            cur.execute(sql, ticket) 
        return cur.lastrowid
    
    
    def update_user_chat_id(conn, user): 
        """
        update chat_id of a user
        :param conn:
        :param user:
        """
        sql = ''' UPDATE users
                  SET chat_id = ? 
                  WHERE user_id = ?'''
        cur = conn.cursor()
        with conn:
            cur.execute(sql, user)
            
    def delete_user(conn, user_id):
        """
        Delete a task by task id
        :param conn:  Connection to the SQLite database
        :param id: id of the user
        :return:
        """
        sql = 'DELETE FROM users WHERE user_id=?'
        cur = conn.cursor()
        with conn:
            cur.execute(sql, (user_id,))
            
    def delete_all_users(conn):
        """
        Delete all rows in the users table
        :param conn: Connection to the SQLite database
        :return:
        """
        sql = 'DELETE FROM users'
        cur = conn.cursor()
        with conn:
            cur.execute(sql)
            
    def delete_all_tickets(conn):
        """
        Delete all rows in the users table
        :param conn: Connection to the SQLite database
        :return:
        """
        sql = 'DELETE FROM tickets'
        cur = conn.cursor()
        with conn:
            cur.execute(sql)
        
            
    def select_user_name_by_id(conn, chat_id):
        """
        Query user by id
        :param conn: the Connection object
        :param id:
        :return:
        """
        cur = conn.cursor()
        with conn:
            cur.execute("SELECT name FROM users WHERE chat_id=?", (chat_id,))
     
        name = str(cur.fetchone())
        
        delete_list = ['(',')',"'",',']
        for word in delete_list:
            name = name.replace(word, "")
     
        return name
    
    def select_user_id_by_chat_id(conn, chat_id):
        """
        Query user by id
        :param conn: the Connection object
        :param id:
        :return:
        """
        cur = conn.cursor()
        with conn:
            cur.execute("SELECT user_id FROM users WHERE chat_id=?", (chat_id,))
     
        user_id = str(cur.fetchone())
        
        delete_list = ['(',')',"'",',']
        for word in delete_list:
            user_id = user_id.replace(word, "")
             
        return user_id
    
    def select_user_id_by_name_and_surname(conn, name_surname): 
        """
        Query user by surname
        :param conn: the Connection object
        :param surname:
        :return:
        """
        name_surname = name_surname.replace(" ", "").lower()
        
        logging.info(name_surname)
        
        cur = conn.cursor()
        with conn:
            cur.execute("SELECT user_id FROM users WHERE  name || surname  =? OR surname || name  =?", (name_surname,name_surname,))
     
        user_id = str(cur.fetchone())
        
        delete_list = ['(',')',"'",',']
        for word in delete_list:
            user_id = user_id.replace(word, "")
     
        return user_id
    
    def select_user_name_by_chat_id(conn, chat_id): 
        """
        Query user by id
        :param conn: the Connection object
        :param surname:
        :return:
        """
        cur = conn.cursor()
        with conn:
            cur.execute("SELECT name FROM users WHERE chat_id=?", (chat_id,))
     
        name = str(cur.fetchone())
        
        delete_list = ['(',')',"'",',']
        for word in delete_list:
            name = name.replace(word, "")
        
        return name
    
    def vacuum(conn):
        """
        Delete all rows in the users table
        :param conn: Connection to the SQLite database
        :return:
        """
        sql = "DELETE FROM sqlite_sequence WHERE name = 'users'"
        cur = conn.cursor()
        with conn:
            cur.execute(sql)
    
def main():
    database = "pythonsqlite.db" 
    
    conn = sqlite3.connect(database)
    
    sql_create_users_table = """ CREATE TABLE IF NOT EXISTS users (
                                    user_id integer PRIMARY KEY AUTOINCREMENT,
                                    chat_id text,
                                    name text NOT NULL,
                                    surname text NOT NULL,
                                    role text NOT NULL
                                ); """
 
    sql_create_tickets_table = """CREATE TABLE IF NOT EXISTS tickets (
                                    ticket_id integer PRIMARY KEY AUTOINCREMENT,
                                    user_id integer NOT NULL,
                                    chat_id integer NOT NULL,
                                    timestamp text NOT NULL,
                                    category text NOT NULL,
                                    corpus text NOT NULL,
                                    feedback text,
                                    add_feedback text,
                                    FOREIGN KEY (user_id) REFERENCES users (user_id)
                                );"""
    
    DBHelper.create_table(conn, sql_create_tickets_table)   

    # create a database connection
    if conn is not None:
        
        #cleanup database
        DBHelper.delete_all_users(conn)
        DBHelper.delete_all_tickets(conn)
        DBHelper.vacuum(conn)

        # create projects table
        DBHelper.create_table(conn, sql_create_users_table)
        # create tasks table
        DBHelper.create_table(conn, sql_create_tickets_table)   
                
        # insert a user
        # create a new user
        user_1 = (None,None, 'erik', 'bessegato','Software Engineer');
        user_2 = (None,None, 'germano', 'rizzo','Innovation Manager');
        DBHelper.insert_user(conn,user_1)
        DBHelper.insert_user(conn,user_2)

    else:
        print("Error! cannot create the database connection.")

if __name__ == '__main__':
    main()