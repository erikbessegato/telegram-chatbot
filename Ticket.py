# timestamp libraries
import time
import datetime

class Ticket:
    
    def __init__(self, user_id):
        self.user_id = user_id
        self.chat_id = None
        self.timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        self.count = 0
        self.category = None
        self.corpus = None
        self.feedback = None
        self.add_feedback = None
        
    def asArray(self):
        arr = (None,self.user_id,self.chat_id,self.timestamp,self.category,self.corpus,self.feedback,self.add_feedback)
        return arr
        
    def reset(self):
        self.timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        self.count = 0
        self.category = None
        self.corpus = None
        self.feedback = None
        self.add_feedback = None 